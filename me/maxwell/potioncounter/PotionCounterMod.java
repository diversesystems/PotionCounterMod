package me.maxwell.potioncounter;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import net.minecraft.client.Minecraft;
import net.minecraft.command.ICommand;
import me.maxwell.potioncounter.settings.CommandSettings;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.Mod;

@Mod(name = "PotionCounterMod", modid = "potioncountermod", version = "1.0")
public class PotionCounterMod
{
    public static int posX;
    public static int posY;
    
    @Mod.EventHandler
    public void onFMLInitialization(final FMLInitializationEvent event) {
        loadSettings();
        MinecraftForge.EVENT_BUS.register((Object)new PotionCountRenderer());
        FMLCommonHandler.instance().bus().register((Object)new PotionCountRenderer());
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandSettings());
    }
    
    private static void loadSettings() {
        final File settings = new File(Minecraft.getMinecraft().mcDataDir, "potioncounter.settings");
        if (!settings.exists()) {
            return;
        }
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(settings));
            final String[] options = reader.readLine().split(":");
            PotionCounterMod.posX = Integer.valueOf(options[0]);
            PotionCounterMod.posY = Integer.valueOf(options[1]);
            reader.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }
    
    public static void saveSettings() {
        final File settings = new File(Minecraft.getMinecraft().mcDataDir, "potioncounter.settings");
        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter(settings));
            writer.write(PotionCounterMod.posX + ":" + PotionCounterMod.posY);
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    static {
        PotionCounterMod.posX = 0;
        PotionCounterMod.posY = 0;
    }
}
