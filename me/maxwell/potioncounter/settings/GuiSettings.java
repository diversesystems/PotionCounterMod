package me.maxwell.potioncounter.settings;

import net.minecraft.client.gui.Gui;
import me.maxwell.potioncounter.utils.InventoryUtils;
import me.maxwell.potioncounter.PotionCounterMod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraft.client.gui.GuiScreen;

import java.io.IOException;

public class GuiSettings extends GuiScreen
{
    private boolean isDragging;
    private int lastX;
    private int lastY;
    
    public GuiSettings() {
        this.isDragging = false;
        this.lastX = 0;
        this.lastY = 0;
    }
    
    public void display() {
        MinecraftForge.EVENT_BUS.register((Object)this);
        FMLCommonHandler.instance().bus().register((Object)this);
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        MinecraftForge.EVENT_BUS.unregister((Object)this);
        FMLCommonHandler.instance().bus().unregister((Object)this);
        Minecraft.getMinecraft().displayGuiScreen((GuiScreen)this);
    }
    
    public void drawScreen(final int x, final int y, final float partialTicks) {
        super.drawDefaultBackground();
        Gui.drawRect(PotionCounterMod.posX, PotionCounterMod.posY, PotionCounterMod.posX + this.mc.fontRendererObj.getStringWidth((InventoryUtils.getPotionsFromInventory() == 1) ? (InventoryUtils.getPotionsFromInventory() + " §cpot") : (InventoryUtils.getPotionsFromInventory() + " §cpots")) + 4, PotionCounterMod.posY + 12, 1140850688);
        this.mc.fontRendererObj.drawString((InventoryUtils.getPotionsFromInventory() == 1) ? (InventoryUtils.getPotionsFromInventory() + " §cpot") : (InventoryUtils.getPotionsFromInventory() + " §cpots"), PotionCounterMod.posX + 2, PotionCounterMod.posY + 2, -1);
        super.drawScreen(x, y, partialTicks);
    }
    
    protected void mouseClicked(final int x, final int y, final int time)throws IOException {
        final int minX = PotionCounterMod.posX;
        final int minY = PotionCounterMod.posY;
        final int maxX = PotionCounterMod.posX + this.fontRendererObj.getStringWidth("1 CPS") + 4;
        final int maxY = PotionCounterMod.posY + 12;
        if (x >= minX && x <= maxX && y >= minY && y <= maxY) {
            this.isDragging = true;
            this.lastX = x;
            this.lastY = y;
        }
        super.mouseClicked(x, y, time);
    }
    
    protected void mouseReleased(final int x, final int y, final int which) {
        if (which == 0 && this.isDragging) {
            this.isDragging = false;
        }
        super.mouseReleased(x, y, which);
    }
    
    protected void mouseClickMove(final int x, final int y, final int lastButtonClicked, final long timeSinceClick) {
        if (this.isDragging) {
            PotionCounterMod.posX += x - this.lastX;
            PotionCounterMod.posY += y - this.lastY;
            this.lastX = x;
            this.lastY = y;
        }
        super.mouseClickMove(x, y, lastButtonClicked, timeSinceClick);
    }
    
    public void onGuiClosed() {
        PotionCounterMod.saveSettings();
    }
    
    public boolean doesGuiPauseGame() {
        return false;
    }
}
