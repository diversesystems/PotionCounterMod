package me.maxwell.potioncounter;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.Gui;
import me.maxwell.potioncounter.utils.InventoryUtils;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.RenderGameOverlayEvent;

public class PotionCountRenderer
{
    @SubscribeEvent
    public void onRenderGameOverlay(final RenderGameOverlayEvent.Post event) {
        if (event.getType() != RenderGameOverlayEvent.ElementType.EXPERIENCE || event.isCancelable()) {
            return;
        }
        if (Minecraft.getMinecraft().currentScreen != null || Minecraft.getMinecraft().gameSettings.showDebugInfo) {
            return;
        }
        Gui.drawRect(PotionCounterMod.posX, PotionCounterMod.posY, PotionCounterMod.posX + Minecraft.getMinecraft().fontRendererObj.getStringWidth((InventoryUtils.getPotionsFromInventory() == 1) ? (InventoryUtils.getPotionsFromInventory() + " §cpot") : (InventoryUtils.getPotionsFromInventory() + " §cpots")) + 4, PotionCounterMod.posY + 12, 1140850688);
        Minecraft.getMinecraft().fontRendererObj.drawString((InventoryUtils.getPotionsFromInventory() == 1) ? (InventoryUtils.getPotionsFromInventory() + " §cpot") : (InventoryUtils.getPotionsFromInventory() + " §cpots"), PotionCounterMod.posX + 2, PotionCounterMod.posY + 2, -1);
    }
}
