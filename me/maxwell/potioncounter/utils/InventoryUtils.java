package me.maxwell.potioncounter.utils;

import java.util.Iterator;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSplashPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.item.ItemPotion;
import net.minecraft.client.Minecraft;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;

public class InventoryUtils
{
    public static int getPotionsFromInventory() {

        int count = 0;
        for (int i = 0; i < Minecraft.getMinecraft().thePlayer.inventory.mainInventory.length; i++) {
            if(Minecraft.getMinecraft().thePlayer.inventory.mainInventory[i] != null)
                if (!Minecraft.getMinecraft().thePlayer.inventory.mainInventory[i].isStackable()) {
                    final ItemStack is = Minecraft.getMinecraft().thePlayer.inventory.mainInventory[i];
                    final Item item = is.getItem();
                    if (item instanceof ItemSplashPotion) {
                        final ItemSplashPotion potion = (ItemSplashPotion)item;
                        if (potion.hasEffect(is)) {
                            for (final Object o : PotionUtils.getEffectsFromStack(is)) {
                                final PotionEffect effect = (PotionEffect)o;
                                if (effect.getPotion() == (Potion) Potion.REGISTRY.getObject(new ResourceLocation("instant_health"))) {
                                    ++count;
                                }
                            }
                        }
                    }
                }
        }
        return count;
    }
}
